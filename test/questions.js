var assert = require("assert"),
	app = require('../app'),
	answer = require('../app/service/answer');

describe('Anwer Module', function() {
	describe('Question 1', function() {
		it('should return 0 with no data', function() {
			assert.equal(answer.solveQuestion1(), 0);
		})

		it('should return 0 with no females', function() {
			assert.equal(answer.solveQuestion1([
				{ name: 'Homer', gender: 'Male', age: new Date()}
			]), 0);
		})

		it('should return 2 with 2 Females and 1 Male', function() {
			assert.equal(answer.solveQuestion1([
				{ name: 'Marge', gender: 'Female', age: new Date()}
				,{ name: 'Lisa', gender: 'Female', age: new Date()}
				,{ name: 'Homer', gender: 'Male', age: new Date()}
			]), 2);
		})
	})

	describe('Question 2', function() {
		it('should return with false if no data', function() {
			assert.equal(answer.solveQuestion2(), false);
		})

		it('should return Address Object if called with only 1 data', function() {
			var date = new Date();

			var answerToQ2 = answer.solveQuestion2([
				{ name: 'Homer', gender: 'Male', age: date}
			]);

			// assert doesn't compare objects properly
			assert.equal(answerToQ2, 'Homer');
		})

		it('should return Homer if Homer is the oldest', function() {
			var answerToQ2 = answer.solveQuestion2([
				{ name: 'Lisa', gender: 'Female', age: new Date(99,1,1)}
				,{ name: 'Marge', gender: 'Female', age: new Date(77,1,1)}
				,{ name: 'Homer', gender: 'Male', age: new Date(66,1,1)}
			]);

			assert.equal(answerToQ2, 'Homer');
		})
	})

	describe('Question 3', function() {
		it('should return false if no data given', function() {
			assert.equal(answer.solveQuestion3(), false);
		})
		
		it('should return false if Paul or Bill are not in data', function() {
			assert.equal(answer.solveQuestion3([
				{ name: 'Homer', gender: 'Male', age: new Date()}
				,{name: 'Marge', gender: 'Female', age: new Date()}
			]), false);
		})

		it('should return 0 if Paul and Bill are on the same date', function() {
			var paulBillDate = new Date();

			assert.equal(answer.solveQuestion3([
				{ name: 'Paul Robinson', gender: 'Male', age: paulBillDate}
				, { name: 'Bill McKnight', gender: 'Male', age: paulBillDate}
			]), 0);
		})

		it('should return 1 if Paul is 1 day older than Bill', function() {
			assert.equal(answer.solveQuestion3([
				{ name: 'Paul Robinson', gender: 'Male', age: new Date(2015,1,2)}
				, { name: 'Bill McKnight', gender: 'Male', age: new Date(2015,1,1)}
			]), 1);
		});

		it('should return 10 if Paul is 10 day older than Bill', function() {
			assert.equal(answer.solveQuestion3([
				{ name: 'Paul Robinson', gender: 'Male', age: new Date(2015,1,11)}
				, { name: 'Bill McKnight', gender: 'Male', age: new Date(2015,1,1)}
			]), 10);
		});
	})
})