var assert = require("assert"),
	app = require('../app'),
	adress = require('../app/service/adress');

var correctFileContent = "Bill McKnight, Male, 16/03/77\nPaul Robinson, Male, 15/01/85\nGemma Lane, Female, 20/11/91\nSarah Stone, Female, 20/09/80\nWes Jackson, Male, 14/08/74";

describe('Adress Loading', function() {
	describe('Parsing the Content String', function() {
		it('should be of type array', function() {
			var data = adress.parseContent(correctFileContent);

			assert(Array.isArray(data), 'data must be of type array');
		});

		it('should build an array of 5 items', function() {
			var data = adress.parseContent(correctFileContent);

			assert(data.length === 5, 'data must have 5 items');
		});

		it('should contain Array of Objects with name/gender/age', function() {
			var data = adress.parseContent(correctFileContent);

			assert(Object.keys(data[0]), ['name', 'gender', 'age'], 'data must contain of Objects with name, gender, age');
		});

		it('should have the data stored in the adress object', function() {
			adress.getAdressContent('./adresses.txt');

			var checkData = adress.parseContent(correctFileContent);
			
			assert(adress.data, checkData, 'File Content must be stored in the adress module');
		});
	});
});
