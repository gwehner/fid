# Installation
```bash
npm install
```
in your cloned folder

# Starting the App
```bash
npm start
```
in your cloned folder

Navigate to [http://localhost:3000/](http://localhost:3000/)

# Testing the App
 * Install mocha
```bash
npm install -g mocha
```
 * run the test
```bash
mocha
```