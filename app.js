

var express = require('express'),
  config = require('./config/config'),
  adress = require('./app/service/adress');

var app = express();

require('./config/express')(app, config);

app.listen(config.port, adress.getAdressContent('./adresses.txt'));

