var express = require('express'),
  router = express.Router();

var adress = require('../service/adress');
var answer = require('../service/answer');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/questions', function(request, response, next) {
	response.render('questions/index', {
		answers: [
			answer.solveQuestion1(adress.data),
			answer.solveQuestion2(adress.data),
			answer.solveQuestion3(adress.data)
		]
	});
});

router.get('/questions/1', function(request, response, next) {
	response.render('questions/1', {
		answer: answer.solveQuestion1(adress.data)
	});
});

router.get('/questions/2', function(request, response, next) {
	response.render('questions/2', {
		answer: answer.solveQuestion2(adress.data)
	});
});

router.get('/questions/3', function(request, response, next) {
	response.render('questions/3', {
		answer: answer.solveQuestion3(adress.data)
	});
});

