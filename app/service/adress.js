var fs = require('fs');

module.exports = {
	data: null,

	readFile: function(filename) {
		var me = this;

		return fs.readFileSync(filename, 'utf8');
	},

	parseContent: function(contentString) {
		var parts = contentString.split("\n");
		var parsedContent = parts.map(function(item) {
			var subParts = item.split(",");

			var dateParts = subParts[2].trim().split("/");
			var dateObject = new Date(dateParts[2], dateParts[1], dateParts[0]);

			return {
				name: subParts[0].trim(),
				gender: subParts[1].trim(),
				age: dateObject
			};
		});

		return parsedContent;
	},

	getAdressContent: function(filename) {
		var me = this;

		var content = me.readFile(filename);

		me.data = me.parseContent(content);
	}
};