module.exports = {
	questions: [
		{ text: 'How many women are in the address book?', solvingFunction: this.solveQuestion1 }
		,{ text: 'Who is the oldest person in the address book?', solvingFunction: this.solveQuestion2 }
		,{ text: 'How many days older is Bill than Paul?', solvingFunction: this.solveQuestion3 }
	]

	,solveQuestion1: function(addresses) {
		if(typeof addresses === 'undefined') {
			return 0;
		}

		if(Array.isArray(addresses)) {
			var filtered = addresses.filter(function(item) {
				return item.gender === 'Female';
			});

			return filtered.length;
		}

		return 0;
	}

	,solveQuestion2: function(addresses) {
		if(typeof addresses === 'undefined') {
			return false;
		}

		if(Array.isArray(addresses)) {
			if(addresses.length > 0) {
				var sorted = addresses.sort(function(a, b) {
					if(a.age < b.age) {
						return -1;
					} else if (a.age > b.age) {
						return 1;
					} else {
						return 0;
					}
				});

				return sorted[0].name;
			} else {
				return false;
			}
		}
	}

	,solveQuestion3: function(addresses) {
		if(typeof addresses === 'undefined') {
			return false;
		}

		if(Array.isArray(addresses)) {
			var billFilter = addresses.filter(function(item) {
				return item.name === 'Bill McKnight';
			});

			var paulFilter = addresses.filter(function(item) {
				return item.name === 'Paul Robinson';
			});

			if(billFilter.length === 0 || paulFilter.length === 0) {
				return false;
			}

			// Paul and Bill must be found
			var paulDate = paulFilter[0].age;
			var billDate = billFilter[0].age;

			// getTime is in millisec
			var day = 60 * 60 * 24 * 1000;

			return Math.round((paulDate.getTime() - billDate.getTime()) / day);
		}
	}

};